# This file is part of the ChemEM software.
#
# Copyright (c) 2023 - Topf Group & Leibniz Institute for Virology (LIV),
# Hamburg, Germany.
#
# This module was developed by:
#   Aaron Sweeney    <aaron.sweeney AT cssb-hamburg.de>
from ChemEM.messages import Messages
from ChemEM.tools.rdtools import RDTools
import os
import numpy as np 



class SignificantFeatures:
    
    def __init__(self, system):
        #inside atoms 
        #atom radii
        self.system = system
        
        
    
    def get_inside_atom_data(self):
        included_residue_keys = self.system.segment.proteins 
        atom_coords = []
        atom_radii = []
        for num, prot in enumerate(included_residue_keys):
            for residue_key in prot:
                residue = self.system.proteins[num].get_residue(residue_key)
                for atom in residue:
                    #TODO! Backbone
                    
                    
                    if atom.element != 'H':
                        atom_coords.append(atom.coords)
                        atom_radii.append(RDTools.get_van_der_waals_radius(atom.element))
        
        self.atom_coords = np.array(atom_coords)
        self.atom_radii = np.array(atom_radii)
                
        
    
    def run(self):
        import pdb 
        pdb.set_trace()
        print(Messages.difference_map())
        self.get_inside_atom_data()